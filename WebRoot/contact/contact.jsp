<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">

<title>欢迎来到IdeaBox</title>

<!-- 新 Bootstrap 核心 CSS 文件 -->
<link rel="stylesheet"
	href="/ideabox/lib/bootstrap/css/bootstrap.min.css">
<!-- 可选的Bootstrap主题文件（一般不用引入） -->
<link rel="stylesheet"
	href="/ideabox/lib/bootstrap/css/bootstrap-theme.min.css">
<!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
<script src="/ideabox/lib/jquery/jquery-1.11.1.min.js"></script>
<script src="/ideabox/lib/bootstrap/js/bootstrap.min.js"></script>
<!-- Custom styles for this template -->
<link href="/ideabox/lib/cover.css" rel="stylesheet">

</head>
<body>
	<div class="site-wrapper">
		<header class="navbar navbar-inverse headroom"
		id="ideaboxnav" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/ideabox/index.jsp">IdeaBox</a>
			</div>
			<ul class="nav navbar-nav">
				<s:if test="#session.user!= null">
					<li><a href="/ideabox/questionnaire/QuesTitle.jsp">创建问卷</a>
				</s:if>
				<li><a href="/ideabox/questionnaire/QuestionList.action"
					class="navbar-link">参与问卷</a></li>
				<li><a href="/ideabox/contact/contact.jsp" class="navbar-link">联系我们</a></li>
			</ul>
			<div class="navbar-collapse collapse">
				<s:if test="#session.user == null">
					<form action="./login/login.action"
						class="navbar-form navbar-right" role="form">
						<div class="form-group">
							<input type="text" name="email" placeholder="Email"
								class="form-control">
						</div>
						<div class="form-group">
							<input type="password" name="password" placeholder="密码"
								class="form-control">
						</div>

						<button type="submit" class="btn btn-success">登录</button>
						<a class="btn btn-success" href="/ideabox/signup/signup.jsp">
							注册</a>
					</form>
			</div>
			<!--/.navbar-collapse -->
			</s:if>
			<s:else>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-right navbar-nav">
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown"><s:property
									value="#session.user.Username" /> <span class="caret"></span>
						</a>
							<ul class="dropdown-menu" role="menu" style="text-shadow: none">
								<s:if test="#session.user.Uid != 1">
									<li><a href="/ideabox/userdashboard/UserDashboard!listAnswered.action">已回答的问卷</a>
									</li>
									<li class="divider"></li>
									<li><a href="/ideabox/userdashboard/UserDashboard!listPublished.action">已发布的问卷</a>
									</li>
									
									<li class="divider"></li>
									<li><a href="/ideabox/userdashboard/EditInformation!view.action?isView=true">个人信息管理</a>
									</li>
									<li class="divider"></li>
									<li><a href="/ideabox/login/logout.action">登出</a>
									</li>
									</s:if>
								<s:else>
									<li><a
										href="/ideabox/admindashboard/AdminDashboard!listUser.action">管理用户</a>
									</li>
									<li class="divider"></li>
									<li><a
										href="/ideabox/admindashboard/AdminDashboard!listQues.action">管理问卷</a>
									</li>

									<li class="divider"></li>
									<li><a
										href="/ideabox/userdashboard/EditInformation!view.action?isView=true">个人信息管理</a>
									</li>
									<li class="divider"></li>
									<li><a href="/ideabox/login/logout.action">登出</a></li>
								</s:else>
							</ul>
						</li>
					</ul>
				</div>
			</s:else>
		</div>
	</header>

		<div class="cover-container">
			<div class="inner cover">

				<h1 class="cover-heading">
					<small> your idea</small>IdeaBox<small>we care</small><br /> <small>倾听你的声音</small>
				</h1>

				<div class="row">
					<div class="col-md-1"></div>
					<div class="col-md-1" style="font-size:36px;">
						<br />&nbsp;&nbsp;DEV:
					</div>
					<h2 class="col-md-4">
						<div style="font-size:30px" align="right">SM:李旭</div>
						<div style="font-size:5px" align="center">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font
								style="font-size:22px;color: #777;box-sizing: border-box;text-align: center;text-shadow: 0 1px 3px rgba(0,0,0,.5);">
								11211087 </font>
						</div>
					</h2>
					<br />
					<h2 class="col-md-5">
						<br />PM:包嘉伟<small>11211099</small>
					</h2>
				</div>
				<div class="row">
					<div class="col-md-1"></div>
					<h3 class="col-md-4">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;黄永招<small>11211044</small>
					</h3>
					<h4 class="col-md-3">
						金晶<small>11211029</small>
					</h4>
					<h2 class="col-md-4">
						郭哲聪<small>11211098</small>
					</h2>
				</div>
				<div class="row">
					<h3 class="col-md-1"></h3>
					<h2 class="col-md-4">
						付鹏<small>11211093</small>
					</h2>
					<h3 class="col-md-4">
						王春元<small>11211090</small>
						<div style="font-size:24px" align="left">&nbsp;&nbsp;&nbsp;&nbsp;一</div>
						<div style="font-size:24px" align="left">
							&nbsp;&nbsp;&nbsp;&nbsp;潇 <font
								style="font-size:16px;color: #777;box-sizing: border-box;text-align: center;text-shadow: 0 1px 3px rgba(0,0,0,.5);">
								11211086 </font>
						</div>
					</h3>
					<h3>
						<br /> 贝祥舜<small>11211114</small>
					</h3>
				</div>

				<h4>Email:&nbsp;ideabox2014@163.com</h4>
				<hr>
				<h4>我们或许会忘记怎么写网站, 但我们不会忘记那年夏天我们是多么的快乐和努力。</h4>
				<h4>2014.&nbsp;6.&nbsp;30&nbsp;&nbsp;~&nbsp;&nbsp;2014.&nbsp;7.&nbsp;11</h4>
			</div>
		</div>

		<br /> <br /> <br />
		<div class="footer">
			<div class="container">
				<!-- <p>用对世界满满的爱来做Design和Coding</p>-->
				<p>
					Code licensed under <a
						href="http://www.apache.org/licenses/LICENSE-2.0" target="_blank">Apache
						License v2.0</a>, documentation under <a
						href="http://creativecommons.org/licenses/by/3.0/">CC BY 3.0</a>.
				</p>
			</div>
		</div>
	</div>

</body>
</html>